<p align="center"><img src="mehdi.jpg" width="100" heigth="100"></p>

# Laravel messenger app built with vue js, pusher and rtl bootstrap made by mehdi hasanpour

this project is a messenger which made by laravel in backend and vue in front-end

## Getting Started

Clone the repository. The repository contains a Laravel project. You can run the project with this command:

```
php artisan serve
```

### Prerequisites

You need the following installed:

* [PHP](https://www.php.net/manual/en/install.php/)
* [Laravel](https://laravel.com/docs/5.8/installation)

## Built With

* [Laravel](https://laravel.com/) - Used to build the backend of the web app
* [Vue.js](https://vuejs.org/) - Used to frontend of the web app

* this project supports rtl language 

* we use vazir font in this contact manager
